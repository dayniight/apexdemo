import React, {Component} from 'react';
import './App.scss';
import Chart from 'react-apexcharts'

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value : '',
      optionsCourbe: {
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: 'smooth'
        },


        xaxis: {
          type: 'day',
          categories: ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        },
        tooltip: {
          x: {
            format: 'dd/MM/yy HH:mm'
          },
        }
      },
      seriesCourbe: [{
        name: 'Mon forfait annuel',
        data: [31, 40, 28, 51, 42, 109, 100]
      }, {
        name: 'TGV Max',
        data: [11, 32, 45, 32, 34, 52, 41]
      } , {
        name: 'IDTGVMAX 2',
        data: [24, 54, 15, 17, 33, 88, 99]
      }],

      options: {
        chart: {
          toolbar: {
          show: true,
          }
        },
        plotOptions: {
          radialBar: {
            offsetX: -20,
            startAngle: 0,
            endAngle: 270,
            hollow: {
              margin: 0,
              size: '60%',
              background: 'transparent',
              image: undefined,
            },
            dataLabels: {
              name: {
                show: false,

              },
              value: {
                show: false,
              }
            }
          }
        },
        colors: ['#4C448F', '#00E396', '#008FFB'],
        labels: ['Mon forfait annuel', 'IDTGVMAX 2', 'TGV Max'],
        legend: {
          show: true,
          floating: true,
          fontSize: '12px',
          position: 'left',
          offsetX: 0,
          offsetY: 0,
          labels: {
              useSeriesColors: false,
          },
          markers: {
              size: 0
          },
          formatter: function(seriesName, opts) {
              return seriesName + ":  " + opts.w.globals.series[opts.seriesIndex]
          },
          itemMargin: {
              horizontal: 0,
          }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    show: false
                }
            }
        }]
      },
      series: [76, 67, 61],
      series2: [55, 41, 85],
      
      optionsColumn: {
        colors: ['#4C448F', '#00E396', '#008FFB'],
        chart: {
          stacked: true,
          stackType: '100%'
        },
        responsive: [{
          breakpoint: 8000,
          options: {
            legend: {
              position: 'bottom',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        xaxis: {
          categories: ['Perte/vol', 'SAV 2', 'SAV 4', 'SAV 5'],
        },
        fill: {
          opacity: 1
        },
        legend: {
          position: 'right',
          offsetX: 0,
          offsetY: 50
        }
      },
      seriesColumn: [{
        name: 'IDTGVMAX',
        data: [44, 55, 41, 67,]
      }, {
        name: 'TGV Max',
        data: [13, 23, 20, 8,]
      }, {
        name: 'Mon forfait annuel',
        data: [11, 17, 15, 15,]
      }],
    }
  }

  ChangeSeries = (option) => {
    this.setState({      
      value: option.target.value
    })
    if(option.target.value === 'day') {
      this.setState({
        optionsCourbe: {
          xaxis: {
            type: 'month',
            categories: ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
          },
        }, 
        seriesCourbe: [{
          data: [88,	45,	28,	82,	44,	69,	40]
        }, {
          data: [97,	81,	69,	34,	59,	82,	63]
        }, {
          data: [75,	99,	68,	22,	48,	38,	9]
        }]
      })
    }
    else if(option.target.value === 'month') {

      this.setState({
        optionsCourbe: {
          xaxis: {
            type: 'month',
            categories: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet"]
          },
        }, 
        seriesCourbe: [{
          data: [2, 43, 55, 51, 42, 11, 56]
        }, {
          data: [11, 32, 45, 32, 34, 52, 41]
        }, {
          data: [24, 54, 15, 17, 33, 88, 99]
        }]
      })
    }
    else {
      this.setState({
        optionsCourbe: {
          xaxis: {
            type: 'years',
            categories: ["2013", "2014", "2015", "2016", "2017", "2018", "2019"]
          },
        },    
        seriesCourbe: [{
          data: [47,	99,	19,	42,	57,	58,	49]
        }, {
          data: [90,	92,	99,	40,	40,	32,	35]
        }, {
          data: [9,	10,	30,	55,	49,	12,	22]
        }]
      })
    }
  }

  render() {
    return (
      <div className="app">
        <h1>Tableau de bord</h1>
        <div className="content">
          <div className="chart radialBar">
            <h2>Contrats en circulation</h2>
            <Chart 
              options={this.state.options} 
              series={this.state.series} 
              type="radialBar" 
              height="400"
              width="400"
            />
          </div>
          <div className="chart chartLong">
            <h2>Nouvelles souscriptions</h2>
            <div className="period">
              <p>Periode :</p>
              <select onChange={this.ChangeSeries}>
                <option value="day">Quotidienne</option>
                <option value="month">Mensuel</option>
                <option value="years">Annuel</option>
              </select> 
            </div>
            <Chart 
              options={this.state.optionsCourbe}
              series={this.state.seriesCourbe}
              type="area"
              width="550"
              height="350"
            />
          </div>
          <div className="chart radialBar">
            <h2>Nombre d’abonné en suspension</h2>
            <Chart 
              options={this.state.options} 
              series={this.state.series2} 
              type="radialBar" 
              height="400"
              width="400"
            />
          </div>
          <div className="chart chartLong">
            <h2>Nouvelles souscriptions</h2>
            <Chart 
              options={this.state.optionsColumn}
              series={this.state.seriesColumn}
              type="bar"
              width="550"
              height="350"
            />
          </div>
        </div>
      </div>
    );
  }
}
  


export default App;
